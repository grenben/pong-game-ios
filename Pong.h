//
//  Pong.h
//  LabiOSPong
//
//  Created by ITHS on 2016-02-21.
//  Copyright © 2016 johan. All rights reserved.
//

#import <UIKit/UIKit.h>

int Y;
int X;
int ComputerScoreNumber;
int PlayerScoreNumber;

@interface Pong : UIViewController {
    
    IBOutlet UIImageView *Ball;
    IBOutlet UIButton *StartButton;
    IBOutlet UIImageView *Player;
    IBOutlet UIImageView *Computer;
    
    IBOutlet UILabel *PlayerScore;
    IBOutlet UILabel *ComputerScore;
    IBOutlet UILabel *WinOrLoose;
    IBOutlet UIButton *Exit;
    
    NSTimer *timer;
    
}

-(IBAction)StartButtonPressed:(id)sender;
-(void)BallMovment;
-(void)ComputerMovement;
-(void)Collision;

@end
