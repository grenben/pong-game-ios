//
//  Pong.m
//  LabiOSPong
//
//  Created by ITHS on 2016-02-21.
//  Copyright © 2016 johan. All rights reserved.
//

#import "Pong.h"

@interface Pong ()

@end

@implementation Pong

CGFloat width;
CGFloat height;

-(void)Collision {
    
    if (CGRectIntersectsRect(Ball.frame, Player.frame)) {
        
        Y = arc4random() %5;
        Y = 0 - Y;
    }
    
    if (CGRectIntersectsRect(Ball.frame, Computer.frame)) {
        
        Y  = arc4random() %5;
        
    }
    
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    UITouch *Drag = [[event allTouches] anyObject];
    Player.center = [Drag locationInView:self.view];
    
    if (Player.center.y > height - 60) {
        Player.center = CGPointMake(Player.center.x, height - 60);
    }
    
    if (Player.center.y < height - 60) {
        Player.center = CGPointMake(Player.center.x, height - 60);
    }
    
    if (Player.center.x < 60) {
        Player.center = CGPointMake(60, Player.center.y);
    }
    
    if (Player.center.x > width - 60) {
        Player.center = CGPointMake(width - 60, Player.center.y);
    }
    
}

-(void)ComputerMovement {
    
    if (Computer.center.x > Ball.center.x) {
        Computer.center = CGPointMake(Computer.center.x - 2, Computer.center.y);
    }
    
    if (Computer.center.x < Ball.center.x) {
        Computer.center = CGPointMake(Computer.center.x + 2, Computer.center.y);
    }
    
    if (Computer.center.x < 55) {
        Computer.center = CGPointMake(55, Computer.center.y);
    }
    
    if (Computer.center.x > width - 55) {
        Computer.center = CGPointMake(width - 55, Computer.center.y);
    }
    
}



- (IBAction)StartButtonPressed:(id)sender {
    
    StartButton.hidden = YES;
    
    Y = arc4random() %11;
    Y = Y - 5;
    
    X = arc4random() %11;
    X = X - 5;
    
    if (Y == 0) {
        Y = 1;
    }
    
    if (X == 0) {
        X = 1;
    }
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(BallMovment) userInfo:nil repeats:YES];

}


-(void)BallMovment {
    
    [self ComputerMovement];
    [self Collision];
    
    
    Ball.center = CGPointMake(Ball.center.x + X, Ball.center.y + Y);
    
    if (Ball.center.x < 20) {
        X = 0 - X;
    }
    
    if (Ball.center.x > width - 20) {
        X = 0 - X;
    }
    
//   
//    if (Ball.center.y < 20) {
//        Y = 0 - Y;
//    }
//    
//    if (Ball.center.y > height - 20) {
//        Y = 0 - Y;
//    }
    
    if (Ball.center.y < 0) {
        PlayerScoreNumber = PlayerScoreNumber + 1;
        PlayerScore.text = [NSString stringWithFormat:@"%i", PlayerScoreNumber];
        
        [timer invalidate];
        StartButton.hidden = NO;
        
        Ball.center = CGPointMake(width / 2, height / 2);
        Computer.center = CGPointMake(width / 2, 60);
        
        if (PlayerScoreNumber == 10) {
            StartButton.hidden = YES;
            Exit.hidden = NO;
            WinOrLoose.hidden = NO;
            WinOrLoose.text = [NSString stringWithFormat:@"YOU WIN."];
        }
    }
    
    if (Ball.center.y > height) {
        
        ComputerScoreNumber = ComputerScoreNumber + 1;
        ComputerScore.text = [NSString stringWithFormat:@"%i", ComputerScoreNumber];
        
        [timer invalidate];
        StartButton.hidden = NO;
        
        Ball.center = CGPointMake(width / 2, height / 2);
        Computer.center = CGPointMake(width / 2, 60);
        
        if (ComputerScoreNumber == 10) {
            StartButton.hidden = YES;
            Exit.hidden = NO;
            WinOrLoose.hidden = NO;
            WinOrLoose.text = [NSString stringWithFormat:@"YOU LOOSE."];
        }
        
    }
       
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    width = [UIScreen mainScreen].bounds.size.width;
    height = [UIScreen mainScreen].bounds.size.height;
    
    ComputerScore.hidden = NO;
    PlayerScore.hidden = NO;
    
    
    PlayerScoreNumber = 0;
    ComputerScoreNumber = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
