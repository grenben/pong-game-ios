//
//  AppDelegate.h
//  LabiOSPong
//
//  Created by ITHS on 2016-02-19.
//  Copyright © 2016 johan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

